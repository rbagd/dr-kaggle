---
title: "Study on failed repayments"
output: ioslides_presentation
---

```{r globalopts, include = FALSE}
knitr::opts_chunk$set(echo = FALSE,
                      message = FALSE,
                      warning = FALSE,
                      comment = NA,
                      fig.align = "center")
```

```{r setup}
library(xgboost)
library(ggplot2)
library(ROCR)

## Read in CSV file
raw_data <- read.csv("./cs-training.csv")

data <- data.frame(
  "failure" = raw_data$SeriousDlqin2yrs,
  "rel.balance" = raw_data$RevolvingUtilizationOfUnsecuredLines,
  "age" = raw_data$age,
  "debt" = raw_data$DebtRatio,
  "income" = ifelse(raw_data$MonthlyIncome == 0, 0, log(raw_data$MonthlyIncome)),
  "loans" = raw_data$NumberOfOpenCreditLinesAndLoans,
  "loans.re" = raw_data$NumberRealEstateLoansOrLines,
  "deps" = raw_data$NumberOfDependents,
  "late30" = raw_data$NumberOfTime30.59DaysPastDueNotWorse,
  "late60" = raw_data$NumberOfTime60.89DaysPastDueNotWorse,
  "late90" = raw_data$NumberOfTimes90DaysLate)

pos.data <- data[!is.na(data$income) & data$income > 0,]
```

## Objectives

**Primary objective**

* Predict whether a client will default on a new loan given his past financial history and demographic variables

**Secondary objectives**

* Determine the factors which signal a potential to default as well as the mechanism through which the client may find himself in poor financial situation
* Construct a client-wise indicator of risk to default


## Data quality issues

Out of `r nrow(data)` available observations, some obvious problems were detected with the dataset.

* Monthly income data
    * `r sum(is.na(raw_data$MonthlyIncome))` clients with missing data for monthly income
    * `r sum(raw_data$MonthlyIncome <= 1, na.rm = TRUE)` clients with income at 0 or 1
* Low number of "failure to repay" cases
    * only `r round(sum(raw_data$SeriousDlqin2yrs) / nrow(raw_data) * 100, 2)` % of clients are classified as having experienced a default. If unaccounted for, this might cause imbalances in statistical modelling.
* Suspicious subgroup of 139 clients
    * Clients in this subgroup have (almost) identical financial records

## Variable importance in prediction


```{r data, fig.width = 8}
load("xg.RData")
imp.table <- head(imp.table, 10)
imp.table$Feature <- c("Revolving util.", "Debt ratio", "Income", "Age",
                       "No. loans", "Late 30-59 d.",
                       "Late 90 d.", "Inv. debt",
                       "Late 60-89 d.", "Dependents")
imp.table$Feature <- factor(imp.table$Feature, levels = imp.table$Feature)

ggplot(imp.table,
       aes(x = Feature, y = Gain, fill = Cover)) +
  theme_bw() +
  geom_bar(stat = "identity") +
  ylab("Gain / Importance") + xlab("") +
  scale_fill_continuous("Relative no. of observations\nrelated to the feature") +
  theme(legend.position = "bottom")
```

## Decision factors

* Currently, **revolving utilization of credit lines** as well as **debt ratio** are identified as strong predictors.
    * Intuitively, these variables make sense.
    * In modelling terms, their mechanism of action is not yet fully clear.
* **Age** and **income** are negatively correlated with failure to repay.
* **Higher number of past dues in previous loans** is a very strong signal of potential failure to repay.


##

```{r exploratory, fig.width = 8}
ggplot(pos.data,
       aes(x = income, y = debt, col = factor(failure))) +
  theme_bw() +
  geom_point() +
  xlab("log(Monthly income)") + ylab("Debt ratio") +
  scale_color_discrete("Failed to repay",
                       labels = c("No", "Yes")) +
  scale_y_log10() +
  facet_wrap(~ cut(late30, breaks = c(0, 1, 2, Inf), include.lowest = TRUE))
```

Plots are split by the number of times a client has been 30-59 days past due for his previous loans.

## Prediction accuracy

```{r falserates, fig.cap = "<strong>True negatives</strong> are clients that were predicted correctly in failing to repay a loan. <strong>False negatives</strong> are clients who repaid their loan but were predicted not to."}
## False negative vs. true negative
rates <- c("fnr", "tnr")

perf_xg <- performance(pred_xg, rates[1], rates[2])
plot(perf_xg, colorkey = TRUE, colorize = TRUE,
     downsampling = 100)
```


## Next steps

1. Find and discuss solutions or work-arounds to the data quality issues
2. Investigate furtherly relation between revolving utilization, debt and failure to repay loans. This relation is not picked up by standard models.
3. Refine the current predictive model
    * Discuss with internal people on potential new predictors that can be added
    * Clustering of clients according to financials or demographics
    * Further parameter tuning
    
    Predictive accuracy of the model can still likely be improved.
